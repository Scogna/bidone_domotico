package com.example.progettoparte3.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.progettoparte3.CardItem;
import com.example.progettoparte3.Database.CardItemRepository;

import java.util.List;

/**
 * The ViewModel class is designed to store and manage UI-related data in a lifecycle conscious way.
 * The ViewModel class allows data to survive configuration changes such as screen rotations.
 *
 * The data stored by ViewModel are not for long term. (Until activity is destroyed)
 *
 * This ViewModel is linked to the HomeFragment, which represents the list of items
 */
public class ListItemViewModel extends AndroidViewModel {

    private LiveData<List<CardItem>> item_list;

    public ListItemViewModel(@NonNull Application application) {
        super(application);
        CardItemRepository repository = new CardItemRepository(application);
        item_list = repository.getItems();
    }

    public LiveData<List<CardItem>> getItems() {
        return item_list;
    }
}
