package com.example.progettoparte3.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progettoparte3.R;

/**
 * A ViewHolder describes an item view and the metadata about its place within the RecyclerView.
 * Every item in the list has a listener for the onclick event
 */
class CardViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

    ImageView imageCardView;
    TextView placeTextView;
    TextView descriptionTextView;
    TextView dateTextView;
    ImageView shareButton;
    private OnItemListener itemListener;

    CardViewHolder(@NonNull View itemView, OnItemListener lister) {
        super(itemView);
        imageCardView = itemView.findViewById(R.id.placeImage);
        placeTextView = itemView.findViewById(R.id.placeTextView);
        descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
        dateTextView = itemView.findViewById(R.id.dateTextView);
        shareButton = itemView.findViewById(R.id.shareIcon);
        itemListener = lister;

        itemView.setOnClickListener(this);
    }

    /**
     * implementation of method in View.OnclickListener
     * @param v the view
     */
    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAdapterPosition());
    }
}