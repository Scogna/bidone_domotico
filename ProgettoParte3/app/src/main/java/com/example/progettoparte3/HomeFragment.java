package com.example.progettoparte3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progettoparte3.RecyclerView.CardAdapter;
import com.example.progettoparte3.RecyclerView.OnItemListener;
import com.example.progettoparte3.ViewModel.ListItemViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Objects;

/**
 * Fragment included in the HomePage
 */
public class HomeFragment extends Fragment implements OnItemListener {

    private static final String LOG = "Home-Fragment_LAB";
    private CardAdapter adapter;
    private RecyclerView recyclerView;

    /**
     * Called to have the fragment instantiate its user interface view.
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    /**
     * Called when the fragment's activity has been created and this fragment's view hierarchy instantiated.
     * It can be used to do final initialization once these pieces are in place,
     * such as retrieving views or restoring state.
     *
     * @param savedInstanceState If the fragment is being re-created from a previous saved state, this is the state.
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            // Set up the toolbar
            Utility.setUpToolbar((AppCompatActivity) getActivity(), getString(R.string.app_name));
            setRecyclerView(activity);

            ListItemViewModel model = new ViewModelProvider(activity).get(ListItemViewModel.class);
            //when the list of the items changed, the adapter gets the new list.
            model.getItems().observe(activity, new Observer<List<CardItem>>() {
                @Override
                public void onChanged(List<CardItem> cardItems) {
                    adapter.setData(cardItems);
                }
            });
            //add action for the FAB button
            FloatingActionButton floatingActionButton = activity.findViewById(R.id.fab_add);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivityForResult(new Intent(activity, AddActivity.class),
                            Utility.ACTIVITY_ADD_TRIP);
                }
            });

        } else {
            Log.e(LOG, "Activity is null");
        }
    }

    /**
     * Called to do initial creation of a fragment.
     * Note that this can be called while the fragment's activity is still in the process of being created.
     * @param savedInstanceState  If the fragment is being re-created from a previous saved state, this is the state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //show the Toolbar menu (the search icon in our case)
        setHasOptionsMenu(true);
    }

    /**
     * Initialize the contents of the Fragment host's standard options menu.
     * You should place your menu items in to menu. For this method to be called,
     * you must have first called setHasOptionsMenu(boolean) in onCreateView.
     * @param menu, The options menu in which you place your items
     * @param inflater, the inflater
     */
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.toolbar_menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            /**
             * Called when the user submits the query. This could be due to a key press on the keyboard
             * or due to pressing a submit button.
             * @param query the query text that is to be submitted
             * @return true if the query has been handled by the listener, false to let the
             * SearchView perform the default action.
             */
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            /**
             * Called when the query text is changed by the user.
             * @param newText the new content of the query text field.
             * @return false if the SearchView should perform the default action of showing any
             * suggestions if available, true if the action was handled by the listener.
             */
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }

        });
    }

    /**
     * Method to set the RecyclerView and the relative adapter
     * @param activity the current activity
     */
    private void setRecyclerView(final Activity activity){
        // Set up the RecyclerView
        recyclerView = activity.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        final OnItemListener listener = this;
        adapter = new CardAdapter(listener, activity);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Method called from the MainActivity when the user add a new card
     */
    void updateList() {
        Log.d(LOG, "updateList()");
        adapter.updateList();
    }

    /**
     * Method called when the user click a card
     * @param position position of the item clicked
     */
    @Override
    public void onItemClick(final int position) {
        final View view = Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(position)).itemView;
        final CardView cardView = view.findViewById(R.id.single_card);
        final View expandableView = cardView.findViewById(R.id.expandableView);

        //transition to show the hidden layout of the card clicked
        TransitionManager.beginDelayedTransition(cardView, new Slide());
        expandableView.setVisibility(expandableView.getVisibility()== View.GONE ?
                View.VISIBLE : View.GONE );
    }
}
