package com.example.progettoparte3.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.progettoparte3.CardItem;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {CardItem.class}, version = 1, exportSchema = false)
abstract class CardItemDatabase extends RoomDatabase {
     abstract CardItemDAO cardItemDAO();

    //Singleton instance
    private static volatile CardItemDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;

    //ExecutorService with a fixed thread pool that you will use to run database operations
    // asynchronously on a background thread.
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    //get the singleton instance
    static CardItemDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CardItemDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CardItemDatabase.class, "item_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}