package com.example.progettoparte3.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progettoparte3.CardItem;
import com.example.progettoparte3.R;
import com.example.progettoparte3.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter linked to the RecyclerView of the homePage, that extends a custom ViewHolder and
 * implements Filterable for the SearchView
 */
public class CardAdapter extends RecyclerView.Adapter<CardViewHolder> implements Filterable {

    private static final String LOG = "CardAdapter-LAB";
    //list that can be filtered
    private List <CardItem> cardItemList = new ArrayList<>();
    //list that contains ALL the element added by the user
    private List <CardItem> cardItemListFull = new ArrayList<>();

    //listener attached to the onclick event for the item in yhe RecyclerView
    private OnItemListener listener;

    //i need the activity to get the drawable for the image
    private Activity activity;

    public CardAdapter(OnItemListener listener, Activity activity) {
        this.listener = listener;
        this.activity = activity;
    }

    /**
     *
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     *
     * @param parent ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,
                parent, false);
        return new CardViewHolder(layoutView, listener);
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * This method should update the contents of the RecyclerView.ViewHolder.itemView to reflect
     * the item at the given position.
     *
     * @param holder ViewHolder which should be updated to represent the contents of the item at
     *               the given position in the data set.
     * @param position position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        final CardItem currentCardItem = cardItemList.get(position);
        String imagePath = currentCardItem.getImageResource();
        if (imagePath.contains("ic_")) {
            Drawable drawable = activity.getDrawable(activity.getResources()
                    .getIdentifier(currentCardItem.getImageResource(), "drawable",
                            activity.getPackageName()));
            holder.imageCardView.setImageDrawable(drawable);
        } else {
            Bitmap bitmap = Utility.getImageBitmap(activity, Uri.parse(imagePath));
            if (bitmap != null){
                holder.imageCardView.setImageBitmap(bitmap);
            }
        }

        holder.placeTextView.setText(currentCardItem.getPlace());
        holder.descriptionTextView.setText(currentCardItem.getDescription());
        holder.dateTextView.setText(currentCardItem.getDate());

        holder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, v.getContext().getString(R.string.place) +
                        currentCardItem.getPlace() + "\n" + v.getContext().getString(R.string.description) +
                        currentCardItem.getDescription() + "\n" + v.getContext().getString(R.string.date)
                        + currentCardItem.getDate());

                sendIntent.setType("text/plain");
                if (v.getContext() != null &&
                        sendIntent.resolveActivity(v.getContext().getPackageManager()) != null) {
                    v.getContext().startActivity(Intent.createChooser(sendIntent, null));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    /**
     * Method called when you have to filter a list (in our case the one with the trip
     * @return  filter that can be used to constrain data with a filtering pattern.
     *
     */
    @Override
    public Filter getFilter() {
        return cardFilter;
    }

    public void updateList() {
        Log.d(LOG, "updateList()");
        notifyDataSetChanged();
    }

    private Filter cardFilter = new Filter() {
        /**
         * Called to filter the data according to the constraint
         * @param constraint constraint used to filtered the data
         * @return the result of the filtering
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<CardItem> filteredList = new ArrayList<>();

            //if you have no constraint --> return the full list
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(cardItemListFull);
            } else {
                //else apply the filter and return a filtered list
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (CardItem item : cardItemListFull) {
                    if (item.getDescription().toLowerCase().contains(filterPattern) ||
                            item.getPlace().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        /**
         * Called to publish the filtering results in the user interface
         * @param constraint constraint used to filter the data
         * @param results the result of the filtering
         */
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cardItemList.clear();
            //cardItemList.addAll((List) results.values);
            List<?> result = (List<?>) results.values;
            for (Object object : result) {
                if (object instanceof CardItem) {
                    cardItemList.add((CardItem) object);
                }
            }
            //warn the adapter that the dare are changed after the filtering
            notifyDataSetChanged();
        }
    };

    /**
     * Method called when a new item is added
     * @param newData the new list of items
     */
    public void setData(List<CardItem> newData) {
        this.cardItemList.clear();
        this.cardItemList.addAll(newData);
        this.cardItemListFull.clear();
        this.cardItemListFull.addAll(newData);
        notifyDataSetChanged();
    }
}
