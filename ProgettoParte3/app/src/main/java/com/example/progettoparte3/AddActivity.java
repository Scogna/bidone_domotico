package com.example.progettoparte3;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.progettoparte3.Utilities.InternetUtilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static com.example.progettoparte3.Utility.REQUEST_IMAGE_CAPTURE;

public class AddActivity extends AppCompatActivity {

    private Uri currentPhotoUri;
    private Bitmap imageBitmap;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        // Instantiate the RequestQueue.
        requestQueue = Volley.newRequestQueue(this);

        //if there was no configuration change, create the fragment
        if (savedInstanceState == null) {
           Utility.insertFragment(this, new AddFragment(), "addFragment");
        } else {
            //otherwise get the uri of the photo taken and set it to the ImageView
            currentPhotoUri = savedInstanceState.getParcelable("currentPhotoUri");
            imageBitmap = savedInstanceState.getParcelable("image");
            AddFragment fragment = (AddFragment) getSupportFragmentManager().findFragmentByTag("addFragment");
            if (fragment != null) {
                fragment.setImageView(imageBitmap);
            }
        }
    }

    /**
     * Called after the picture is taken
     * @param requestCode requestcode od the intent
     * @param resultCode result of the intent
     * @param data data of the intent (picture)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
                try {
                    if (imageBitmap != null) {
                        //method to save the image in the gallery of the device
                        saveImage(imageBitmap);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("LAB", String.valueOf(currentPhotoUri));
            // Load a specific media item, and show it in the ImageView
            Bitmap bitmap = Utility.getImageBitmap(this, currentPhotoUri);
            if (bitmap != null){
                ImageView imageView = findViewById(R.id.imageView);
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    /**
     * Method called to save the image taken as a file in the gallery
     * @param bitmap the image taken
     * @throws IOException if there are some issue with the creation of the image file
     */
    private void saveImage(Bitmap bitmap) throws IOException {
        // Create an image file name
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        String name = "JPEG_" + timeStamp + "_.png";

        ContentResolver resolver = getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        currentPhotoUri = imageUri;
        OutputStream fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

        //for the jpeg quality, it goes from 0 to 100
        //for the png one, the quality is ignored
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        if (fos != null) {
            fos.close();
        }
    }

    //get the URI of the photo
    Uri getCurrentPhotoUri(){
        return currentPhotoUri;
    }

    /**
     * Method called before a configuration change
     * @param outState bundle where saved elements are stored
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //save the bitmap and the uri of the image taken
        outState.putParcelable("image", imageBitmap);
        outState.putParcelable("currentPhotoUri", currentPhotoUri);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register the callback that keep monitored the internet connection
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null) {
            //cancel the request to osm
            requestQueue.cancelAll(InternetUtilities.OSM_REQUEST_TAG);
        }
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            //unregistered the callback
            connectivityManager.unregisterNetworkCallback(InternetUtilities.getNetworkCallback());
        }
    }

    /**
     * Method called to query the osm API
     * @param latitude latitude of the device
     * @param longitude longitude of the device
     */
    void createRequest(String latitude, String longitude){
        String url ="https://nominatim.openstreetmap.org/reverse?lat="+latitude+
                "&lon="+longitude+"&format=json&limit=1";

        final AddFragment fragment = (AddFragment) getSupportFragmentManager().findFragmentByTag("addFragment");
        // Request a jsonObject response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (fragment != null) {
                                fragment.setTextView(response.get("display_name").toString());
                            }
                        } catch (JSONException e) {
                            fragment.setTextView("/");
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });

        jsonObjectRequest.setTag(InternetUtilities.OSM_REQUEST_TAG);
        // Add the request to the RequestQueue.
        requestQueue.add(jsonObjectRequest);
    }
}
