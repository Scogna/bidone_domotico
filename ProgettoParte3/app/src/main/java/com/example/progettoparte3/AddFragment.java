package com.example.progettoparte3;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.progettoparte3.Location.LocationUpdatesBroadcastReceiver;
import com.example.progettoparte3.Utilities.InternetUtilities;
import com.example.progettoparte3.Utilities.UtilitiesGPS;
import com.example.progettoparte3.Utilities.UtilityPermissions;
import com.example.progettoparte3.ViewModel.AddItemViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.example.progettoparte3.Utility.REQUEST_IMAGE_CAPTURE;

public class AddFragment extends Fragment {

    private static final String LOG = "Add-Fragment_LAB";

    private AddItemViewModel model;

    private TextInputEditText placeTextInputEditText;
    private TextInputEditText descriptionTextInputEditText;
    private TextInputEditText dateTextInputEditText;

    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;

    /**
     * BroadcastReceiver that receives the data about the location of the device from the
     * LocationUpdatesBroadcastReceiver
     */
    private BroadcastReceiver broadcastReceiver =  new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals("SEND_DATA")) {
                String lat = intent.getStringExtra("location_latitude");
                String lon = intent.getStringExtra("location_longitude");
                Activity activity = getActivity();
                if (activity != null) {
                    //check the connection
                    if (InternetUtilities.getIsNetworkConnected()) {
                        //if device is connected, create the request
                        ((AddActivity) getActivity()).createRequest(lat, lon);
                    } else {
                        //otherwise the snackbar is shown
                        InternetUtilities.getSnackbar().show();
                    }
                }
                //once I have the location, I can remove the update for the location
                removeLocationUpdates();
            }
        }
    };

    private Bitmap bitmap;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_add, container, false);

        //if there was a configuration change, i retrieve the image if it was taken
        if (savedInstanceState != null){
            ImageView imageView = view.findViewById(R.id.imageView);
            imageView.setImageBitmap(bitmap);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Activity activity = getActivity();
        if (activity != null) {
            // Set up the toolbar
            Utility.setUpToolbar((AppCompatActivity) getActivity(), getString(R.string.add_trip));

            placeTextInputEditText = activity.findViewById(R.id.placeTextInputEditText);
            descriptionTextInputEditText = activity.findViewById(R.id.descriptionTextInputEditText);
            dateTextInputEditText = activity.findViewById(R.id.dateTextInputEditText);


            // Check if the user revoked runtime gps permissions.
            if (!UtilityPermissions.checkPermissions(getActivity())) {
                UtilityPermissions.requestPermissions(getActivity());
            }
            // set the information about location update
            setLocation(activity);
            //create the snackbar for the internet connection
            InternetUtilities.makeSnackbar(activity);

            model = new ViewModelProvider((ViewModelStoreOwner) activity).get(AddItemViewModel.class);

            final TextInputLayout placeTextInputLayout = activity.findViewById(R.id.placeTextInputLayout);

            //using InputEditText to avoid empty place
            placeTextInputEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int i, KeyEvent keyEvent) {
                    if (inputNotEmpty(placeTextInputEditText.getText())) {
                        placeTextInputLayout.setError(null); //Clear the error
                    } else {
                        placeTextInputLayout.setError(getString(R.string.place_empty));
                    }
                    return false;
                }
            });

            activity.findViewById(R.id.captureButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dispatchTakePictureIntent(activity);
                }
            });


            activity.findViewById(R.id.gpsButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestLocation();
                }
            });

            activity.findViewById(R.id.fab_add).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (inputNotEmpty(placeTextInputEditText.getText())) {
                        //when the image is taken, i save the uri
                       Uri imageUri = ((AddActivity)activity).getCurrentPhotoUri();
                       String imageUriString;
                       if (imageUri == null){
                           //if the image was not taken, i save a drawable
                           imageUriString = "ic_launcher_foreground";
                       } else {
                           imageUriString = imageUri.toString();
                       }
                       Log.d("LAB", imageUriString);
                        //add item to the database using the viewmodel and room
                        model.addItem(new CardItem(imageUriString,
                                placeTextInputEditText.getText().toString(),
                                Objects.requireNonNull(descriptionTextInputEditText.getText()).toString(),
                                Objects.requireNonNull(dateTextInputEditText.getText()).toString()));

                        activity.setResult(RESULT_OK);
                        activity.finish();
                    } else {
                        placeTextInputLayout.setError(getString(R.string.place_empty));
                    }
                }
            });
        } else {
            Log.e(LOG, "Activity is null");
        }
    }

    private boolean inputNotEmpty(@Nullable Editable text) {
        return text != null && text.length() > 0;
    }

    /**
     * Method called when the use click on the take picture button.
     * An intent is created
     * @param activity the activity displayed
     */
    private void dispatchTakePictureIntent(Activity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * Method use to set the bitmap that will be displayed on the ImageView
     * @param bitmap bitmap representing the picture taken.
     */
    void setImageView(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    /**
     * Intent send to the LocationUpdatesBroadcastReceiver, to obtain the location of the device.
     * @return the pendingIntent: A description of an Intent and target action to perform with it
     */
    private PendingIntent getPendingIntent() {
        Intent intent = new Intent(getActivity(), LocationUpdatesBroadcastReceiver.class);
        //Retrieve a PendingIntent that will perform a broadcast
        return PendingIntent
                .getBroadcast(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * Set the information for the location updates, using fusedLocationProviderClient from google
     * @param activity the activity we are in
     */
    private void setLocation(Activity activity){
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
        locationRequest = new LocationRequest();
        // Set the desired interval for active location updates, in milliseconds.
        locationRequest.setInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Called when we have obtained the coordinates of the device to remove
     * future updates of the location
     */
    private void removeLocationUpdates() {
        Log.i("LAB", "Removing location updates");
        fusedLocationProviderClient.removeLocationUpdates(getPendingIntent());
        Activity activity = getActivity();
        if (activity != null) {
            //unregister the receiver because we don't need it anymore
            activity.unregisterReceiver(broadcastReceiver);
        }
    }

    /**
     * Call to obtain the location of the device
     */
    private void requestLocation() {
        try {
            Log.i("LAB", "Starting location updates");
            Activity activity = getActivity();
            if (activity != null) {
                //check if gps is on
                UtilitiesGPS.statusCheck(activity);
                fusedLocationProviderClient.requestLocationUpdates(locationRequest, getPendingIntent());
                //register the receiver from java code, the LocationUpdatesBroadcastReceivers.java is
                //register in the Manifest
                getActivity().registerReceiver(broadcastReceiver, new IntentFilter("SEND_DATA"));
            }
        } catch (SecurityException e) {
            removeLocationUpdates();
            e.printStackTrace();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     * In this case the location permission.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i("LAB", "onRequestPermissionResult");
        if (requestCode == UtilityPermissions.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // user interaction was interrupted
                Log.i("LAB", "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                Log.i("LAB", "PERMISSION_GRANTED");
            } else {
                // Permission denied.
                Activity activity = getActivity();
                if (activity != null){
                    UtilityPermissions.createSnackBar(activity);
                }
            }
        }
    }

    /**
     * set the edittext with the place given by osm
     * @param place string from the result of the request to osm
     */
    void setTextView(String place) {
        placeTextInputEditText.setText(place);
    }
}
