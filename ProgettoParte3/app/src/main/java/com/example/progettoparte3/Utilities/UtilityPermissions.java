package com.example.progettoparte3.Utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.example.progettoparte3.BuildConfig;
import com.example.progettoparte3.R;
import com.google.android.material.snackbar.Snackbar;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class UtilityPermissions {

    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 0;
    private static final String REQUEST_PERMISSIONS = Manifest.permission.ACCESS_FINE_LOCATION;

    public static boolean checkPermissions(Activity activity) {
        int permissionState = ActivityCompat.checkSelfPermission(activity, REQUEST_PERMISSIONS);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermissions(final Activity activity) {
        //returns true if the user has previously denied the request, and returns false if a user
        // has denied a permission and selected the Don't ask again option in the permission request
        // dialog, or if a device policy prohibits the permission.
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity, REQUEST_PERMISSIONS);

        // Provide an additional rationale to the user if he didn't select the "Don't ask again" option.
        if (shouldProvideRationale) {
            Log.i("LAB", "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    activity.findViewById(R.id.fragment_container),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            requestLocation(activity);
                        }
                    })
                    .show();
        } else {
            requestLocation(activity);
        }
    }

    private static void requestLocation(Activity activity) {
        Log.i("LAB", "Requesting permission");
        ActivityCompat.requestPermissions(activity,
                new String[]{REQUEST_PERMISSIONS}, REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    public static void createSnackBar(final Activity activity) {
        // Notify the user via a SnackBar that they have rejected a core permission for the
        // app, which makes the Activity useless.
        Snackbar.make(
                activity.findViewById(R.id.fragment_container),
                R.string.permission_denied_explanation,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Build intent that displays the App settings screen.
                        Intent intent = new Intent();
                        intent.setAction(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",
                                BuildConfig.APPLICATION_ID, null);
                        intent.setData(uri);
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                    }
                })
                .show();
    }

}
