package com.example.progettoparte3.Location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.google.android.gms.location.LocationResult;

import java.util.List;

/**
 * BroadcastReceiver for handling location updates.
 */
public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LocationResult result = LocationResult.extractResult(intent);
        //if the location is not null
        if (result != null) {
            List<Location> locations = result.getLocations();
            String latitude = Double.toString(locations.get(locations.size()-1).getLatitude());
            String longitude = Double.toString(locations.get(locations.size()-1).getLongitude());
            //send the intent to anyone that is listening for the action "SEND_DATA"
            Intent i = new Intent("SEND_DATA");
            i.putExtra("location_latitude", latitude);
            i.putExtra("location_longitude", longitude);
            context.sendBroadcast(i);
        }
    }
}
